import React, {useState} from 'react';
import {
  Container,
  Header,
  Content,
  Form,
  Left,
  Right,
  Body,
  Title,
  Icon,
  Item,
  Input,
  Label,
  Button,
  Text,
  DatePicker,
  Picker,
  View,
} from 'native-base';
import {Formik} from 'formik';
import CustomInput from './fields/CustomInput';
import {StyleSheet} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {ProgressDialog} from 'react-native-simple-dialogs';
import * as EmailValidator from 'email-validator';
import {connect} from 'react-redux';
import {reg} from './actions/auth.actions.reg';
import {useEffect} from 'react';
import firestore from '@react-native-firebase/firestore';

const validation = (values) => {
  let errors = {};

  if (!values.firstname) {
    errors.firstname = 'Required';
  } else if (values.firstname.length > 15) {
    errors.firstname = 'Must be 15 characters or less';
  }

  if (!values.lastname) {
    errors.lastname = 'Required';
  } else if (values.lastname.length > 15) {
    errors.lastname = 'Must be 15 characters or less';
  }

  if (!values.email) {
    errors.email = 'Required';
  } else if (!EmailValidator.validate(values.email)) {
    errors.email = 'Invalid email address.';
  }

  const passwordRegex = /(?=.*[0-9])/;
  if (!values.password) {
    errors.password = 'Required';
  } else if (values.password.length < 8) {
    errors.password = 'Password must be 8 characters long.';
  } else if (!passwordRegex.test(values.password)) {
    errors.password = 'Invalid password. Must contain one number.';
  }

  if (!values.confirmpassword) {
    errors.confirmpassword = 'Required';
  } else if (values.confirmpassword.length <= 8) {
    errors.confirmpassword = 'Must be more than 8 characters!';
  }

  if (values.password !== values.confirmpassword) {
    errors.confirmpassword = 'Password must match';
  }

  console.log(values);
  console.log(errors);

  return errors;
};

const Register = ({authenticating, authenticate}) => {
  useEffect(() => {}, []);

  const [selectedValue, setSelectedValue] = useState({});
  const navigation = useNavigation();

  return (
    <Container>
      <Header full style={styles.headerStyle}>
        <Left />
        <Body>
          <Title>Sign up</Title>
        </Body>
        <Right />
      </Header>
      <Content>
        <ProgressDialog
          visible={authenticating}
          title="authenticating"
          message="Please Wait"
        />
        <Formik
          initialValues={{
            firstname: '',
            lastname: '',
            email: '',
            password: '',
            confirmpassword: '',
          }}
          onSubmit={(values, action) => {
            console.log(values);

            firestore()
              .collection('Users')
              .add({
                firstname: values.firstname,
                lastname: values.lastname,
                email: values.email,
                password: values.password,
                confirmpassword: values.confirmpassword,
              })

              .then(() => {
                console.log('User added!');
              })
              .catch((error) => {
                console.log(error);
              });
          }}
          validate={validation}>
          {({handleSubmit}) => (
            <>
              <Form>
                <CustomInput name="firstname" label="First Name" />
                <CustomInput name="lastname" label="Last Name" />
                <CustomInput name="email" label="Email" />
                <CustomInput
                  name="password"
                  label="Password"
                  secureTextEntry={true}
                />

                <CustomInput
                  name="confirmpassword"
                  label="Confirm Password"
                  secureTextEntry={true}
                />
              </Form>

              <Button full style={styles.buttonStyle} onPress={handleSubmit}>
                <Text>Register</Text>
              </Button>
              <Button
                iconLeft
                full
                style={styles.buttonStyle}
                onPress={() => {
                  navigation.goBack();
                }}>
                <Text>Home</Text>
              </Button>

              <Button
                full
                style={styles.buttonStyle}
                onPress={() => {
                  authenticate(true);
                  setTimeout(() => {
                    authenticate(false);
                  }, 1000);
                }}>
                {authenticating === false && <Text>Trigger Loading</Text>}
                {authenticating === true && <Text>Triggered.</Text>}
              </Button>
            </>
          )}
        </Formik>
      </Content>
    </Container>
  );
};

const styles = StyleSheet.create({
  buttonStyle: {
    marginTop: 10,
    backgroundColor: '#7a3b2e',
  },

  headerStyle: {
    backgroundColor: '#c1502e',
  },

  container: {
    flex: 1,
    height: 60,
    justifyContent: 'space-between',
    alignItems: 'center',
  },
});

const mapDispatch = (dispatch) => {
  return {
    authenticate: () => {
      dispatch(reg());
    },
  };
};

const mapState = (state) => {
  return {
    authenticating: state.reg.authenticating,
  };
};

export default connect(mapState, mapDispatch)(Register);
