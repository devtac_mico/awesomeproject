import React from 'react';
import {Item, Input, Label, Text, Picker} from 'native-base';
import {useField} from 'formik';

const CustomPicker = (props) => {
  const [field, meta, helpers] = useField(props);

  const {setValue} = helpers;

  return (
    <>
      <Item>
        <Picker
          note
          mode="dropdown"
          style={{width: 130, height: 60}}
          selectedValue={field.value}
          onValueChange={(value) => {
            setValue(value);
          }}>
          <Picker.Item label="" />
          <Picker.Item label="Male" value="male" />
          <Picker.Item label="Female" value="female" />
        </Picker>
      </Item>
      {meta.touched && meta.error && <Text>* {meta.error}</Text>}
    </>
  );
};

export default CustomPicker;
