import React from 'react';
import {Item, Input, Label, Text} from 'native-base';
import {useField} from 'formik';

const CustomInput = (props) => {
  const [field, meta, helpers] = useField(props);

  const {setValue} = helpers;

  let secureTextEntry = false;
  let otherProps = {};

  if (props.secureTextEntry) {
    secureTextEntry = true;
  }

  if (meta.touched && meta.error) {
    otherProps.error = true;
  }

  return (
    <>
      <Item floatingLabel {...otherProps}>
        <Label>{props.label}</Label>
        <Input
          value={field.value}
          onChangeText={(text) => {
            setValue(text);
          }}
          secureTextEntry={secureTextEntry}
        />
      </Item>
      {meta.touched && meta.error && <Text>* {meta.error}</Text>}
    </>
  );
};

export default CustomInput;
