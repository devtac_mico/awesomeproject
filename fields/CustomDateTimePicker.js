import React from 'react';
import {Item, Label, Text, DatePicker} from 'native-base';
import {useField} from 'formik';

const CustomDateTimePicker = (props) => {
  const [field, meta, helpers] = useField(props);

  const {setValue} = helpers;

  let otherProps = {};

  return (
    <>
      <Item>
        <Label>{props.label}</Label>
        <DatePicker
          defaultDate={new Date(2018, 4, 4)}
          value={field.value}
          locale={'en'}
          timeZoneOffsetInMinutes={undefined}
          modalTransparent={false}
          animationType={'fade'}
          androidMode={'default'}
          placeHolderText="Select date"
          textStyle={{color: 'green'}}
          placeHolderTextStyle={{color: '#d3d3d3'}}
          disabled={false}
          onDateChange={(value) => {
            setValue(value);
          }}
          onChange={(value) => {
            setValue(value);
          }}
        />
      </Item>
      {meta.touched && meta.error && <Text>* {meta.error}</Text>}
    </>
  );
};

export default CustomDateTimePicker;
