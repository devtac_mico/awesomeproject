import {createStore, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';

import rootReducer from './reducer/index';

const createStoreWithMW = applyMiddleware(thunk)(createStore);

let store;

store = createStoreWithMW(rootReducer);

export default store;
