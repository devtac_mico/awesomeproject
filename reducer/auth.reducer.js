const initialState = {
  authenticating: false,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case 'SET_AUTHENTICATING':
      return {...state, authenticating: action.payload};

    default:
      return {...state};
  }
};
