const initialState = {
  authenticating: false,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case 'SET_REGISTER':
      return {...state, authenticating: action.load};

    default:
      return {...state};
  }
};
