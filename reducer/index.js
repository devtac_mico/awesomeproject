import {combineReducers} from 'redux';
import auth from './auth.reducer';
import reg from './reg.reducers';

const rootReducer = combineReducers({
  auth,
  reg,
});

export default rootReducer;
