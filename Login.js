import React, {Component} from 'react';
import {
  Container,
  Header,
  Content,
  Button,
  Text,
  Form,
  Item,
  Label,
  Input,
  Title,
  Card,
  CardItem,
  Body,
  Thumbnail,
} from 'native-base';
import {Formik} from 'formik';
import CustomInput from './fields/CustomInput';
import {StyleSheet} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {connect} from 'react-redux';
import {ProgressDialog} from 'react-native-simple-dialogs';
import {authenticate} from './actions/auth.actions';
import firestore from '@react-native-firebase/firestore';
import {useEffect} from 'react';
import * as EmailValidator from 'email-validator';

const validation = (values) => {
  let errors = {};

  if (!values.email) {
    errors.email = 'Required';
  } else if (!EmailValidator.validate(values.email)) {
    errors.email = 'Invalid email address.';
  }

  if (!values.password) {
    errors.password = 'Required';
  } else if (values.password.length > 15) {
    errors.password = 'Must be 15 characters or less';
  }

  return errors;
};

const Login = ({authenticating, authenticate}) => {
  const navigation = useNavigation();
  useEffect(() => {}, []);

  const uri =
    'https://static.wikia.nocookie.net/dota2_gamepedia/images/e/e4/Map_Concept_Art1.jpg/revision/latest?cb=20150117180327';

  const _uri =
    'https://i.pinimg.com/originals/50/d5/22/50d5228fcf279e1972317f0f3cbca8f3.jpg';
  return (
    <Container>
      <Header style={styles.headerStyle}>
        <Title full style={styles.extStyle}>
          Log in
        </Title>
      </Header>

      <Content>
        <ProgressDialog
          visible={authenticating}
          title=" authenticating "
          message="Nigga can't ya wait?"
        />
        <Formik
          initialValues={{
            email: '',
            password: '',
          }}
          onSubmit={(values, action) => {
            console.log(values);

            firestore()
              .collection('Users')
              .where('email', '==', values.email)
              .where('password', '==', values.password)
              .get()
              .then((querySnapshot) => {
                querySnapshot.forEach((doc) => {
                  // doc.data() is never undefined for query doc snapshots
                  console.log('logged in');
                });
              });
            setTimeout(() => {
              action.resetForm();
            }, 100);
          }}
          validate={validation}>
          {({handleSubmit}) => (
            <>
              <Form>
                <CustomInput name="email" label="Email" />
                <CustomInput
                  name="password"
                  label="Password"
                  secureTextEntry={true}
                />

                <Button full style={styles.buttonStyle} onPress={handleSubmit}>
                  <Text>Login</Text>
                </Button>

                <Button
                  full
                  style={styles.buttonStyle}
                  onPress={() => {
                    navigation.navigate('Register', {});
                  }}>
                  <Text>Sign up</Text>
                </Button>
                <Button
                  full
                  style={styles.buttonStyle}
                  onPress={() => {
                    authenticate(true);
                    setTimeout(() => {
                      authenticate(false);
                    }, 1000);
                  }}>
                  {authenticating === false && <Text>Trigger Loading</Text>}
                  {authenticating === true && <Text>Triggered.</Text>}
                </Button>
                <Card full style={styles._cardStyle}>
                  <CardItem>
                    <Body>
                      <Thumbnail
                        style={styles.thumbnailStyle}
                        height={135}
                        width={335}
                        square
                        source={{uri: uri}}
                      />
                    </Body>
                  </CardItem>
                </Card>

                <Card full style={styles._cardStyle}>
                  <CardItem>
                    <Body>
                      <Thumbnail
                        style={styles.thumbnailStyle}
                        height={135}
                        width={335}
                        square
                        source={{uri: _uri}}
                      />
                    </Body>
                  </CardItem>
                </Card>
              </Form>
            </>
          )}
        </Formik>
      </Content>
    </Container>
  );
};

const styles = StyleSheet.create({
  textStyle: {
    marginTop: 15,
  },

  headerStyle: {
    backgroundColor: '#c1502e',
  },

  buttonStyle: {
    marginTop: 10,
    backgroundColor: '#7a3b2e',
  },

  _cardStyle: {
    marginTop: 10,
    marginBottom: 10,
    marginRight: 10,
    marginLeft: 10,
  },
});

const mapDispatch = (dispatch) => {
  return {
    authenticate: (payload) => {
      dispatch(authenticate(payload));
    },
  };
};
const mapState = (state) => {
  return {
    authenticating: state.auth.authenticating,
  };
};

export default connect(mapState, mapDispatch)(Login);
