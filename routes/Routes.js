import 'react-native-gesture-handler';
import React from 'react';

import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

import ConfirmScreen from '../screens/ConfirmScreen';
import ConfirmScreen2 from '../screens/ConfirmScreen2';
import Login from '../Login';
import Register from '../Register';

const Stack = createStackNavigator();

const Routes = () => {
  return (
    <>
      <NavigationContainer>
        <Stack.Navigator
          screenOptions={{headerShown: false}}
          initialRouteName="Login">
          <Stack.Screen name="App" component={Login} />
          <Stack.Screen name="ConfirmScreen" component={ConfirmScreen} />
          <Stack.Screen name="Register" component={Register} />
          <Stack.Screen name="ConfirmScreen2" component={ConfirmScreen2} />
        </Stack.Navigator>
      </NavigationContainer>
    </>
  );
};

export default Routes;
