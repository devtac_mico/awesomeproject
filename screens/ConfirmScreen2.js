import React, {useEffect} from 'react';

import {
  Item,
  Label,
  Button,
  Text,
  Header,
  Left,
  Body,
  Right,
  Title,
} from 'native-base';

import {useRoute, useNavigation} from '@react-navigation/native';
import {StyleSheet} from 'react-native';

const ConfirmScreen2 = () => {
  const route = useRoute();
  const navigation = useNavigation();

  console.log('route.params');
  console.log(route.params);

  return (
    <>
      <Header full style={styles._headerStyle}>
        <Left />
        <Body>
          <Title>Register</Title>
        </Body>
        <Right />
      </Header>
      <Item>
        <Label style={styles.labelStyle}>Firstname:</Label>
        <Label full style={styles.labelStyle}>
          {route.params.firstname}
        </Label>
      </Item>

      <Item>
        <Label style={styles.labelStyle}>Lastname:</Label>
        <Label full style={styles.labelStyle}>
          {route.params.lastname}
        </Label>
      </Item>

      <Item>
        <Label style={styles.labelStyle}>Email:</Label>
        <Label full style={styles.labelStyle}>
          {route.params.email}
        </Label>
      </Item>

      <Item>
        <Label style={styles.labelStyle}>Password:</Label>
        <Label full style={styles.labelStyle}>
          {route.params.password}
        </Label>
      </Item>

      <Item>
        <Label style={styles.labelStyle}>Confirm Password:</Label>
        <Label full style={styles.labelStyle}>
          {route.params.confirmpassword}
        </Label>
      </Item>

      <Item>
        <Label style={styles.labelStyle}>Birthdate:</Label>
        <Label full style={styles.labelStyle}>
          {route.params.birthdate}
        </Label>
      </Item>

      <Item>
        <Label style={styles.labelStyle}>Birthdate:</Label>
        <Label full style={styles.labelStyle}>
          {route.params.gender}
        </Label>
      </Item>

      <Button
        full
        style={styles.__buttonStyle}
        onPress={() => {
          navigation.goBack();
        }}>
        <Text>Home Page</Text>
      </Button>
    </>
  );
};

const styles = StyleSheet.create({
  labelStyle: {
    marginTop: 20,
  },

  __buttonStyle: {
    backgroundColor: '#c1502e',
  },

  _headerStyle: {
    backgroundColor: '#7a3b2e',
  },
});

export default ConfirmScreen2;
