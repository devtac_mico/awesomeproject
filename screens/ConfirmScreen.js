import React, {useEffect} from 'react';

import {
  Item,
  Label,
  Button,
  Text,
  Header,
  Left,
  Body,
  Right,
  Title,
  Container,
  Card,
  CardItem,
  Thumbnail,
  View,
  Content,
  Icon,
} from 'native-base';

import {useRoute, useNavigation} from '@react-navigation/native';
import {Linking, StyleSheet} from 'react-native';

const ConfirmScreen = () => {
  const route = useRoute();
  const navigation = useNavigation();

  console.log('route.params');
  console.log(route.params);

  const uri =
    'https://cdn.vox-cdn.com/thumbor/KSFusNAEyV1TupIMbIXv7N2qs04=/1400x0/filters:no_upscale()/cdn.vox-cdn.com/uploads/chorus_asset/file/6645195/dota-2-logo.0.jpg';

  const _uri =
    'https://images.squarespace-cdn.com/content/v1/59af2189c534a58c97bd63b3/1558109744835-T82F7L80IUKDV9P7Y1TP/ke17ZwdGBToddI8pDm48kNvT88LknE-K9M4pGNO0Iqd7gQa3H78H3Y0txjaiv_0fDoOvxcdMmMKkDsyUqMSsMWxHk725yiiHCCLfrh8O1z5QPOohDIaIeljMHgDF5CVlOqpeNLcJ80NK65_fV7S1USOFn4xF8vTWDNAUBm5ducQhX-V3oVjSmr829Rco4W2Uo49ZdOtO_QXox0_W7i2zEA/Fall+2016+Battle+loading+screen+Dota+2.jpg?format=2500w';

  return (
    <>
      <Container>
        <Content>
          <Header full style={styles._headerStyle}>
            <Left>
              <Button
                transparent
                onPress={() => {
                  navigation.goBack();
                }}>
                <Icon name="arrow-back" />
                <Text>Back</Text>
              </Button>
            </Left>
            <Body>
              <Title>You're in</Title>
            </Body>
            <Right></Right>
          </Header>
          <Card full style={styles._cardStyle}>
            <Item>
              <Label full style={styles.labelStyle}>
                WELCOME:
              </Label>
              <Label full style={styles.labelStyle}>
                {route.params.username}
              </Label>
            </Item>
          </Card>

          <Button
            full
            style={styles.__buttonStyle}
            onPress={() => {
              navigation.goBack();
            }}>
            <Text>Home Page</Text>
          </Button>

          <Card full style={styles._cardStyle}>
            <CardItem
              header
              button
              onPress={() => alert('This is Card Header')}>
              <Text>DOTA2 logo</Text>
            </CardItem>
            <CardItem
              button
              onPress={() =>
                Linking.openURL(
                  'https://cdn.vox-cdn.com/thumbor/KSFusNAEyV1TupIMbIXv7N2qs04=/1400x0/filters:no_upscale()/cdn.vox-cdn.com/uploads/chorus_asset/file/6645195/dota-2-logo.0.jpg',
                )
              }>
              <Body>
                <Thumbnail
                  style={styles.thumbnailStyle}
                  height={135}
                  width={335}
                  square
                  source={{uri: uri}}
                />
              </Body>
            </CardItem>
          </Card>

          <Card full style={styles._cardStyle}>
            <CardItem
              header
              button
              onPress={() => alert('This is Card Header')}>
              <Text>Concept art</Text>
            </CardItem>
            <CardItem
              button
              onPress={() =>
                Linking.openURL(
                  'https://images.squarespace-cdn.com/content/v1/59af2189c534a58c97bd63b3/1558109744835-T82F7L80IUKDV9P7Y1TP/ke17ZwdGBToddI8pDm48kNvT88LknE-K9M4pGNO0Iqd7gQa3H78H3Y0txjaiv_0fDoOvxcdMmMKkDsyUqMSsMWxHk725yiiHCCLfrh8O1z5QPOohDIaIeljMHgDF5CVlOqpeNLcJ80NK65_fV7S1USOFn4xF8vTWDNAUBm5ducQhX-V3oVjSmr829Rco4W2Uo49ZdOtO_QXox0_W7i2zEA/Fall+2016+Battle+loading+screen+Dota+2.jpg?format=2500w',
                )
              }>
              <Body>
                <Thumbnail
                  style={styles.thumbnailStyle}
                  height={135}
                  width={335}
                  square
                  source={{uri: _uri}}
                />
              </Body>
            </CardItem>
          </Card>
        </Content>
      </Container>
    </>
  );
};

const styles = StyleSheet.create({
  labelStyle: {
    marginTop: 15,
    marginBottom: 15,
  },

  __buttonStyle: {
    backgroundColor: '#7a3b2e',
  },

  _headerStyle: {
    backgroundColor: '#c1502e',
  },

  cardStyle: {
    marginTop: 15,
    marginBottom: 15,
    marginRight: 15,
    marginLeft: 15,
  },

  _cardStyle: {
    marginTop: 15,
    marginBottom: 15,
    marginRight: 15,
    marginLeft: 15,
  },
});

export default ConfirmScreen;
